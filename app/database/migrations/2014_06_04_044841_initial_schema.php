<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialSchema extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users', function($table)
        {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->timestamps();
            $table->softDeletes();
        });		

        Schema::create('cities', function($table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('state');
            $table->double('latitude', 9, 6);
            $table->double('longitude', 9, 6);
            $table->unique( array( 'name', 'state' ) );
            $table->enum('status', array('verified', 'unverified'))->default('verified');
            $table->timestamps();
            $table->softDeletes();

        });   

        Schema::create('users_cities_visited', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('city_id')->references('id')->on('cities');
        });                   
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('users_cities_visited');
		Schema::dropIfExists('users');
        Schema::dropIfExists('cities');
	}

}

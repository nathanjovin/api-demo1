<?php 
class UsersCitiesVisitedTableSeeder extends Seeder
{
    /**
     * Randomly generate 100 visits
     * @return [type] [description]
     */
    public function run()
    {
        DB::table('users_cities_visited')->delete();

        // generate 100 random visits
        for( $i = 0; $i < 100; $i++ )
        {
            $user = User::orderBy(DB::raw('RAND()'))->first();
            $city = City::orderBy(DB::raw('RAND()'))->first();

            $user->cities()->save( $city );
        }
    }    
}

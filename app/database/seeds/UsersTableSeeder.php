<?php 
class UsersTableSeeder extends Seeder
{
    /**
     * Populate the users table
     * @return [type] [description]
     */
    public function run()
    {
        DB::table('users')->delete();
        $users = array();
        $users[] = array( 'id' => 1, 'first_name' => 'Andrew',  'last_name' => 'Anderson');
        $users[] = array( 'id' => 2, 'first_name' => 'Henry',   'last_name' => 'Harrison');
        $users[] = array( 'id' => 3, 'first_name' => 'John',    'last_name' => 'Smith');
        $users[] = array( 'id' => 4, 'first_name' => 'Jim',     'last_name' => 'Smith');
        $users[] = array( 'id' => 5, 'first_name' => 'Jose',    'last_name' => 'Gonzales');
        $users[] = array( 'id' => 6, 'first_name' => 'Andrew',  'last_name' => 'Watson');
        $users[] = array( 'id' => 7, 'first_name' => 'Bill',    'last_name' => 'Brown');
        $users[] = array( 'id' => 8, 'first_name' => 'Michael', 'last_name' => 'Jackson');
        $users[] = array( 'id' => 9, 'first_name' => 'Eliscia', 'last_name' => 'Smith');
        $users[] = array( 'id' => 10, 'first_name' => 'Chris',   'last_name' => 'Jones');

        foreach( $users as $user )
        {
            User::create( $user );
        }
    }    
}

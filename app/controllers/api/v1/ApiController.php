<?php
namespace api\v1;

class ApiController extends \BaseController 
{

    // format the user would like the data returned in 
    protected $returnDataFormat = 'json';

    // available return datra formats
    protected $validDataFormats = array( 'json' );

    // array of records to be returned 
    protected $records = array();
    
    // the total number of records in the query set, excluding any offset/pagination settings
    protected $totalRecordCount = array();

    protected $requestBody;

    /**
     * Build some of the parameters that will be common to many API requests
     */
    public function __construct()
    {
        $request = \Request::instance();
        $this->requestBody = json_decode($request->getContent() );
    }

    /**
     * This will handle formatting and returning data for all classes that extend ApiControllers
     * @return [type] [description]
     */
    public function returnData()
    {
        $response = array();
        // format the return data based on the user's request
        switch( $this->returnDataFormat )
        {
            case 'json':
                $response[ 'status' ] = array(
                    'code' => '200',
                    'text' => 'ok',
                    // 'total_records' => $this->records->count()
                    );
                if( $this->records != null )
                {
                    $response[ 'status' ][ 'total_records' ] = $this->records->count();
                    try 
                    {
                        $response[ 'records' ] = $this->records->orderBy( \Input::get( 'orderBy', 'id' ) )->skip( \Input::get( 'firstRecord', 0 ) )->take( \Input::get( 'pageSize', 100 ) )->get();
                    } catch( \Illuminate\Database\QueryException $e )
                    {
                        return $this->returnError( 400, 'Error fetching data', "There was an error fetching the requested data. Please check your query parameters.");
                    }
                }
                return $response;
                break;
            default:
                return $this->returnError( 400, 'Unknown data format', "You requested the data format '" . $this->returnDataFormat . "' which isn't recognized.  Valid formats are: " . implode( $this->validDataFormats ));
                break;
        }
    }

    protected function returnError( $code, $text, $description = '' )
    {
        $status = array(
            'code' => $code,
            'text' => $text,
            'description' => $description
            );
        $response = array( 'status' => $status );
        $response = \Response::json( $response, $code );
        return $response;
    }

    protected function validateRequest()
    {
        if( \Request::isMethod('post') )
        {       
            // if the request body is null we likely had a hard time parsing the json
            if( $this->requestBody == null)
            {
                return $this->returnError( '400', 'Invalid request body', "This request doesn't look like valid JSON. Please make sure you're using quotation marks around your keys." );
            }  
        }  
        return true;      
    }
}
<?php
namespace api\v1;

class UsersController extends ApiController 
{
    // raw request body for POSTs

    // user id passed in the URL
    private $userId;

    // the user we're querying
    private $user;

    // the city object where we're logging a visit
    private $city;

    public function getVisits( $userId )
    {
        $this->userId = $userId;

        // validate the request
        if( $this->validateRequest() !== true )
        {
            return $this->validateRequest();
        }        

        $this->records = $this->user->cities();

        return $this->returnData();
    }
    public function postVisit( $userId, $format = 'json' )
    {
        
        $this->userId = $userId;

        // validate the request
        if( $this->validateRequest() !== true )
        {
            return $this->validateRequest();
        }

        // log the visit
        $this->user->cities()->save( $this->city );
        
        // se the return data format
        $this->returnDataFormat = $format;

        // don't return any records - we're jusck ack'ing the post
        $this->records = null;

        return $this->returnData();
    }

    /**
     * perform several validations on the request
     * @return [type] [description]
     */
    protected function validateRequest()
    {
        
        if( parent::validateRequest() !== true )
        {
            return parent::validateRequest();
        }
        
        // make sure the requested user exists 
        $this->user = \User::find( $this->userId );
        if( $this->user == null )
        {
            return $this->returnError( '400', 'User does not exist', "We could not find a user with ID $this->userId" );
        }
        
        if( \Request::isMethod('post') )
        {           

            // make sure we have the required data points
            if( !isset($this->requestBody->city) || !isset($this->requestBody->state) )
            {
                return $this->returnError( '400', 'City and State required', "To log a user visit, your request body must include a city and state" );
            }

            // make sure the city and state exist
            $this->city = \City::whereName( $this->requestBody->city )->whereState( $this->requestBody->state )->first();
            if( $this->city == null )
            {
                return $this->returnError( '400', 'City does not exist', "We could not find the city '" . $this->requestBody->city . ", " . $this->requestBody->state . "' ");
            }        
        }
        return true;
    }
}
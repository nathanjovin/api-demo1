<?php
namespace api\v1;

class StatesController extends ApiController 
{
    /**
     * Return a list of cities
     * @param  [type] $state  [description]
     * @param  string $format [description]
     * @param  [type] $city   [description]
     * @return [type]         [description]
     */
    public function getCities( $state, $format = 'json', $cityName = null )
    {
        // if they've requested a specific city, fetch only that one
        if( $cityName !== null )
        {
            $this->records = \City::whereState( $state )->whereName( $cityName );
            $city = $this->records->first();

            // unless they've also specified a radius, in which case we return all cities within that radius
            if( is_numeric( \Input::get( 'radius' ) ) && $city !== null )
            {
                $this->records = $city->getCitiesInRadius( \Input::get( 'radius' ) );
            }
        } else
        {
            // fetch all cities in the state
            $this->records = \City::whereState( $state );
        }
        $this->returnDataFormat = $format;

        return $this->returnData();
    }
}
<?php


class User extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

    /**
     * Each user can visit many cities
     * @return [type] [description]
     */
    public function cities()
    {
        return $this->belongsToMany( 'City', 'users_cities_visited', 'user_id', 'city_id')->withTimestamps();        
    }


}

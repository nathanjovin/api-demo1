<?php


class City extends Eloquent {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cities';

    /**
     * Each city can be visited by many users
     * @return [type] [description]
     */
    public function users()
    {
        return $this->belongsToMany( 'User', 'users_cities_visited', 'city_id', 'user_id')->withTimestamps();        
    }

    /**
     * Return Eloquent collection of cities within radius (in miles) of this city
     * @param  [type] $radius [description]
     * @return [type]         [description]
     */
    public function getCitiesInRadius( $radius )
    {
        // convert radius from miles to kilometers
        $radius = 1.60934 * $radius;
        // 6371 = approximate radius of earth in kilometers
        $where = "
        acos(
            sin(
                radians( ? )
            ) * 
            sin(
                radians(latitude)
            ) + 
            cos(
                radians( ? )
            ) *
            cos(
                radians(latitude)
            ) *
            cos(
                radians(longitude) - radians( ? ) 
            )
        ) * 6371 < ?
        ";
        
        return City::whereRaw( $where, array( $this->latitude, $this->latitude, $this->longitude, $radius ) );
    }

}

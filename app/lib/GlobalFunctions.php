<?php 
/**
 * Formatted variable dumping for easy viewing in HTML.  This function takes any number of arguments.  Each argument
 * is a variable to be dumped.  A final boolean argument indicates whether the script should exit after dumping the
 * data.
 *
 * @access  public
 * @param   mixed   $data   Data to display
 * @param   boolean $exit   Optional flag to indicate exit after printing or not
 * @return  void
 */
function pre_dump( $data, $exit = false )
{
    if ( is_null($data) )
    {
        $value = "{NULL}";
    }
    elseif ( $data === true )
    {
        $value = "{TRUE}";
    }
    elseif ( $data === false )
    {
        $value = "{FALSE}";
    }        
    else
    {
        $value = print_r( $data, true );
    }
    
    $output = "<pre>\n$value</pre>\n";
    echo $output;
    
    if ( $exit )
    {
        exit;
    }
}
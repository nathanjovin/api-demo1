<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// setup the available API versions
$apiVersions = array( 'v1' );


// Setup routes for each resource and each API version
foreach( $apiVersions as $version )
{
    Route::group( array('namespace' => 'api\\' . $version ), function() use( $version )
    {
        Route::get('/' . $version . '/states/{state}/cities.{format}/{city?}', 'StatesController@getCities');
        Route::get('/' . $version . '/users/{userId}/visits', 'UsersController@getVisits');
        Route::post('/' . $version . '/users/{userId}/visits', 'UsersController@postVisit');
    });
}

App::missing(function($exception) use( $apiVersions )
{
    // if the API version doesn't exist, tell them so explicitly
    if ( in_array( Request::segment(1), $apiVersions) === false )
    {
        $response = array( 
            'code' => 404, 
            'text' => 'Unknown API version', 
            'description' => "The requested API version, '" . Request::segment(1) . "', does not exist"
            );
        return \Response::json( $response, '404' );

    } 

    // otherwise, return a generic 404
        $response = array( 
            'code' => 404, 
            'text' => 'Path not found', 
            'description' => "The requested path does not exist"
            );
        return \Response::json( $response, '404' );
});

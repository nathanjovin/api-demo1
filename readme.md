API Demo
=========

Small demo of a RESTful API

### Request Methods
**GET /v1/states/<STATE>/cities.json**

Return a list of cities in a given state (by two-character state code)

**GET /v1/states/<STATE>/cities.json/<CITY>?radius=<RADIUS-IN-MILES>**

Return a list of cities within the specified radius of CITY

**GET /v1/users/<USER_ID>/visits**

Return a list of cities that have been visited by user with id USER_ID

**POST /v1/users/<USER_ID>/visits**

Log a visit by user with id USER_ID.  Location visited is specified in JSON format in the body of the request:
```json
{
    "city": "San Diego",
    "state": "CA"
}
```
### Common parameters
##### Ordering
* **orderBy**: Database field to sort the records by, ex. orderBy=name

##### Pagination
* **firstRecord**: The first record to return, ex. firstRecord=10 will begin the result set at the 10th record (default 0)
* **pageSIze**: Number of records per page (default 100)

**Example**: GET /v1/states/<STATE>/cities.json?orderBy=name&firstRecord=10&pageSize=20
### Response Format
All responses will return with relevant HTTP status codes
* **200**: Request was succesful
* **404**: Requested resource not found
* **400**: There was a problem with the request

The body of each response will be in JSON format with a **status** object and optionally a **records** object (for record requests):
```json
{
    "status": 
    {
        "code": 200,
        "text": "ok",
        "description": "some detailed descriptions about the response" // optional
    },
    "records": []
}
```


